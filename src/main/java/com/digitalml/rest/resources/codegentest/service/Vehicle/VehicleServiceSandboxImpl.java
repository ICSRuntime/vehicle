package com.digitalml.rest.resources.codegentest.service.Vehicle;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.Principal;

import com.digitalml.rest.resources.codegentest.service.VehicleService;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.ws.rs.core.SecurityContext;

import org.apache.commons.lang.StringUtils;

/**
 * Sandbox implementation for: Vehicle
 * Lookup vehicle information.
 *
 * @author admin
 * @version 1.0
 *
 */

public class VehicleServiceSandboxImpl extends VehicleService {
	

    public FindVehicleByRegistrationCurrentStateDTO findvehiclebyRegistrationUseCaseStep1(FindVehicleByRegistrationCurrentStateDTO currentState) {
    

        FindVehicleByRegistrationReturnStatusDTO returnStatus = new FindVehicleByRegistrationReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Lookup vehicle information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindVehicleByRegistrationCurrentStateDTO findvehiclebyRegistrationUseCaseStep2(FindVehicleByRegistrationCurrentStateDTO currentState) {
    

        FindVehicleByRegistrationReturnStatusDTO returnStatus = new FindVehicleByRegistrationReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Lookup vehicle information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindVehicleByRegistrationCurrentStateDTO findvehiclebyRegistrationUseCaseStep3(FindVehicleByRegistrationCurrentStateDTO currentState) {
    

        FindVehicleByRegistrationReturnStatusDTO returnStatus = new FindVehicleByRegistrationReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Lookup vehicle information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindVehicleByRegistrationCurrentStateDTO findvehiclebyRegistrationUseCaseStep4(FindVehicleByRegistrationCurrentStateDTO currentState) {
    

        FindVehicleByRegistrationReturnStatusDTO returnStatus = new FindVehicleByRegistrationReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Lookup vehicle information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindVehicleByRegistrationCurrentStateDTO findvehiclebyRegistrationUseCaseStep5(FindVehicleByRegistrationCurrentStateDTO currentState) {
    

        FindVehicleByRegistrationReturnStatusDTO returnStatus = new FindVehicleByRegistrationReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Lookup vehicle information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindVehicleByRegistrationCurrentStateDTO findvehiclebyRegistrationUseCaseStep6(FindVehicleByRegistrationCurrentStateDTO currentState) {
    

        FindVehicleByRegistrationReturnStatusDTO returnStatus = new FindVehicleByRegistrationReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Lookup vehicle information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


	/**
	 * Creates and returns a {@link Method} object using reflection and handles the possible exceptions.
	 * <br/>
	 * Aids with calling the process step method based on the outcome of the control logic
	 * 
	 * @param methodName
	 * @param clazz
	 * @return
	 */
	private Method getMethod(String methodName, Class clazz) {
		Method method = null;
		try {
			method = VehicleService.class.getMethod(methodName, clazz);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}

		return method;
	}
	
	/**
	 * For use when calling provider systems.
	 * <p>
	 * TODO: Implement security logic here
	 */
	protected SecurityContext securityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}