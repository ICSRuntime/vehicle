package com.digitalml.www.information.model.retail;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for TyreDetails:
{
  "type": "object",
  "properties": {
    "width": {
      "type": "string"
    },
    "profile": {
      "type": "string"
    },
    "rim": {
      "type": "string"
    },
    "load": {
      "type": "string"
    }
  }
}
*/

public class TyreDetails {

	@Size(max=1)
	private String width;

	@Size(max=1)
	private String profile;

	@Size(max=1)
	private String rim;

	@Size(max=1)
	private String load;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    width = null;
	    profile = null;
	    rim = null;
	    load = null;
	}
	public String getWidth() {
		return width;
	}
	
	public void setWidth(String width) {
		this.width = width;
	}
	public String getProfile() {
		return profile;
	}
	
	public void setProfile(String profile) {
		this.profile = profile;
	}
	public String getRim() {
		return rim;
	}
	
	public void setRim(String rim) {
		this.rim = rim;
	}
	public String getLoad() {
		return load;
	}
	
	public void setLoad(String load) {
		this.load = load;
	}
}