package com.digitalml.www.information.model.retail;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for Vehicle:
{
  "type": "object",
  "properties": {
    "name": {
      "type": "string"
    },
    "manufacturer": {
      "type": "string"
    },
    "tyres": {
      "type": "array",
      "items": {
        "$ref": "TyreDetails"
      }
    }
  }
}
*/

public class Vehicle {

	@Size(max=1)
	private String name;

	@Size(max=1)
	private String manufacturer;

	@Size(max=1)
	private List<com.digitalml.www.information.model.retail.TyreDetails> tyres;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    name = null;
	    manufacturer = null;
	    tyres = new ArrayList<com.digitalml.www.information.model.retail.TyreDetails>();
	}
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public List<com.digitalml.www.information.model.retail.TyreDetails> getTyres() {
		return tyres;
	}
	
	public void setTyres(List<com.digitalml.www.information.model.retail.TyreDetails> tyres) {
		this.tyres = tyres;
	}
}