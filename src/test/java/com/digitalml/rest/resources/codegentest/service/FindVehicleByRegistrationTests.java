package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Vehicle.VehicleServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.VehicleService.FindVehicleByRegistrationInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.VehicleService.FindVehicleByRegistrationReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class FindVehicleByRegistrationTests {

	@Test
	public void testOperationFindVehicleByRegistrationBasicMapping()  {
		VehicleServiceDefaultImpl serviceDefaultImpl = new VehicleServiceDefaultImpl();
		FindVehicleByRegistrationInputParametersDTO inputs = new FindVehicleByRegistrationInputParametersDTO();
		inputs.setRegistration(null);
		inputs.setOffset(0);
		inputs.setPagesize(50);
		FindVehicleByRegistrationReturnDTO returnValue = serviceDefaultImpl.findvehiclebyRegistration(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}