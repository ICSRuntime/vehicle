package com.digitalml.rest.resources.codegentest.service;
    	
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import java.net.URL;

import org.apache.commons.collections.CollectionUtils;

import java.lang.Object;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.SecurityContext;
import java.security.AccessControlException;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.*;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

// Import any model objects used by the interface

import com.digitalml.rest.resources.codegentest.*;

/**
 * Service: Vehicle
 * Lookup vehicle information.
 *
 * This service has been automatically generated by Ignite
 *
 * @author admin
 * @version 1.0
 *
 */

public abstract class VehicleService {

	private static final Logger LOGGER = LoggerFactory.getLogger(VehicleService.class);

	// Required for JSR-303 validation
	static private ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();

	protected static Mapper mapper;

	static {
		URL configFile = VehicleService.class.getResource("VehicleServiceMappings.xml");
		if (configFile != null) {

			List<String> mappingFiles = new ArrayList<String>();
			mappingFiles.add(configFile.toExternalForm());
			mapper = new DozerBeanMapper(mappingFiles);

		} else {
			mapper = new DozerBeanMapper(); // Use default wildcard mappings only
		}
	}

	protected boolean checkPermissions(SecurityContext securityContext) throws AccessControlException {
		return true;
	}

	/**
	Implements method findvehiclebyRegistration
	
		Gets a collection of vehicle details filtered by Registration
	*/
	public FindVehicleByRegistrationReturnDTO findvehiclebyRegistration(SecurityContext securityContext, FindVehicleByRegistrationInputParametersDTO inputs)  {

		if (LOGGER.isDebugEnabled())
			LOGGER.debug("Entered method findvehiclebyRegistration");

		// Do any security checks
		if (securityContext == null)
			throw new AccessControlException("No SecurityContext available so cannot access findvehiclebyRegistration");

		if (!checkPermissions(securityContext))
			throw new AccessControlException("Insufficient permissions to access findvehiclebyRegistration");

		FindVehicleByRegistrationReturnDTO returnValue = new FindVehicleByRegistrationReturnDTO();
        FindVehicleByRegistrationCurrentStateDTO currentState = new FindVehicleByRegistrationCurrentStateDTO();
        
        // Setup the inputs for the first process step
        mapper.map(inputs, currentState.getInputs());
        
		Object returnDTO = null;
		return returnValue;
	}


    // Supporting Use Case and Process methods

	public abstract FindVehicleByRegistrationCurrentStateDTO findvehiclebyRegistrationUseCaseStep1(FindVehicleByRegistrationCurrentStateDTO currentState);
	public abstract FindVehicleByRegistrationCurrentStateDTO findvehiclebyRegistrationUseCaseStep2(FindVehicleByRegistrationCurrentStateDTO currentState);
	public abstract FindVehicleByRegistrationCurrentStateDTO findvehiclebyRegistrationUseCaseStep3(FindVehicleByRegistrationCurrentStateDTO currentState);
	public abstract FindVehicleByRegistrationCurrentStateDTO findvehiclebyRegistrationUseCaseStep4(FindVehicleByRegistrationCurrentStateDTO currentState);
	public abstract FindVehicleByRegistrationCurrentStateDTO findvehiclebyRegistrationUseCaseStep5(FindVehicleByRegistrationCurrentStateDTO currentState);
	public abstract FindVehicleByRegistrationCurrentStateDTO findvehiclebyRegistrationUseCaseStep6(FindVehicleByRegistrationCurrentStateDTO currentState);


// Supporting Exception classes

// Supporting DTO classes


	/**
	 * Provides a DTO to hold the current state of the orchestration for the operation Find vehicle by Registration.
	 * This allows the state to be easily passed in method calls.
	 */
	public static class FindVehicleByRegistrationCurrentStateDTO {
		
		private FindVehicleByRegistrationInputParametersDTO inputs;
		private FindVehicleByRegistrationReturnDTO returnObject;
		private FindVehicleByRegistrationReturnStatusDTO errorState;
		// DTOs for orchestration
		
		public FindVehicleByRegistrationCurrentStateDTO() {
			initialiseDTOs();
		}

		public FindVehicleByRegistrationCurrentStateDTO(FindVehicleByRegistrationInputParametersDTO inputs) {
			initialiseDTOs();
			this.inputs = inputs;
		}
		
		// Add extra DTOs for steps
		public FindVehicleByRegistrationInputParametersDTO getInputs() {
			return inputs;
		}

		
		public void setErrorState(FindVehicleByRegistrationReturnStatusDTO errorState) {
			this.errorState = errorState;
		}

		public FindVehicleByRegistrationReturnStatusDTO getErrorState() {
			return errorState;
		}

		public FindVehicleByRegistrationReturnDTO getReturnObject() {
			return returnObject;
		}
		
		private void initialiseDTOs() {
			inputs = new FindVehicleByRegistrationInputParametersDTO();
			returnObject = new FindVehicleByRegistrationReturnDTO();
			errorState = new FindVehicleByRegistrationReturnStatusDTO();
		
		}			
	};

	/**
	 * Holds the return value for the operation Find vehicle by Registration
	 */
	public static class FindVehicleByRegistrationReturnDTO {
		private com.digitalml.rest.resources.codegentest.Response response;

		public com.digitalml.rest.resources.codegentest.Response getResponse() {
			return response;
		}

		public void setResponse(com.digitalml.rest.resources.codegentest.Response response) {
			this.response = response;
		}

	};

	/**
	 * Holds the return value for the operation Find vehicle by Registration when an exception has been thrown.
	 */
	public static class FindVehicleByRegistrationReturnStatusDTO {

		private String exceptionMessage;

		public FindVehicleByRegistrationReturnStatusDTO() {
		}

		public FindVehicleByRegistrationReturnStatusDTO(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}

		private com.digitalml.www.information.model.retail.Error error;

		public com.digitalml.www.information.model.retail.Error getError() {
			return error;
		}

		public void setError(com.digitalml.www.information.model.retail.Error error) {
			this.error = error;
		}


		public String getExceptionMessage() {
			return exceptionMessage;
		}

		public void setExceptionMessage(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}
	};

	/**
	 * Holds the input parameters for the operation Find vehicle by Registration in a single DTO which aids
	 * validation and allows the inputs to be easily passed in method calls.
	 */
	public static class FindVehicleByRegistrationInputParametersDTO {


		private String registration;

		private Integer offset;

		private Integer pagesize;

		public String getRegistration() {
			return registration;
		}

		public void setRegistration(String registration) {
			this.registration = registration;
		}

		public Integer getOffset() {
			return offset;
		}

		public void setOffset(Integer offset) {
			this.offset = offset;
		}

		public Integer getPagesize() {
			return pagesize;
		}

		public void setPagesize(Integer pagesize) {
			this.pagesize = pagesize;
		}


		public boolean validate() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<FindVehicleByRegistrationInputParametersDTO>> errors = validator.validate(this);
			return CollectionUtils.isEmpty(errors);
		}

		public List<String> validateReport() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<FindVehicleByRegistrationInputParametersDTO>> errors = validator.validate(this);

			List<String> results = new ArrayList<String>();
			if (CollectionUtils.isNotEmpty(errors))
				for (ConstraintViolation<FindVehicleByRegistrationInputParametersDTO> error : errors) {
					StringBuffer sb = new StringBuffer();
					sb.append(error.getMessage());
					results.add(sb.toString());
				}

			return results;
		}
	};

}